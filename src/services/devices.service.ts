import { DeviceResponse } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

export const fetchDevices = (database: string) => {
  const controller = loadAbort();
  return {
    call: axios.get<DeviceResponse>(`${database}/devices`, {
      signal: controller.signal,
    }),
    controller,
  };
}