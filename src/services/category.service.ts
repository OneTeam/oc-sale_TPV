import { CategoryResponse } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

export const fetchCategories = (database: string) => {
	const controller = loadAbort();
	return {
		call: axios.get<CategoryResponse[]>(
			`${database}/categories`,
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};
