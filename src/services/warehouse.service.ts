import { WarehouseResponse } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

const BASE_URL = import.meta.env.VITE_API_URL;

export const fetchWarehouses = () => {
	const controller = loadAbort();
	return {
		call: axios.get<WarehouseResponse[]>(
			`${BASE_URL}/api/warehouses`,
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};
