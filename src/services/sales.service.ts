import { loadAbort } from '@/utils';
import { ItemRequest, PrintResponse, SaleRequest, SaleResponse } from '@/types';
import axios from 'axios';

const controller = loadAbort();

export const fetchSales = (database: string, device: number) => {
	return {
		call: axios.get<SaleResponse[]>(
			`${database}/sales?device=${device}`,
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};

export const fetchSale = (database: string, id: number) => {
	return {
		call: axios.get<SaleResponse>(`${database}/sales/${id}`, {
			signal: controller.signal,
		}),
		controller,
	};
};

export const startSale = (database: string, request: SaleRequest) => {
	return {
		call: axios.post<SaleResponse>(
			`${database}/sales`,
			request,
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};

export const changeTable = (
	database: string,
	id: number,
	table: number,
) => {
	return {
		call: axios.put<SaleResponse>(
			`${database}/sales/${id}`,
			{ table },
			{ signal: controller.signal },
		),
		controller,
	};
};

export const completeSaleSuccess = (
	database: string,
	id: number,
	payment_method: string,
) => {
	return {
		call: axios.put<PrintResponse>(
			`${database}/sales/${id}/success`,
			{
				payment_method,
			},
			{ 
				signal: controller.signal,
				responseType: 'blob',
			},
		),
		controller,
	};
};

export const printSale = (
	database: string,
	id: number,
) => {
	return {
		call: axios.get<PrintResponse>(
			`${database}/sales/${id}/print`,
			{
				signal: controller.signal,
				responseType: 'blob',
			},
		),
		controller,
	};
};

export const canceledSale = (database: string, id: number) => {
	return {
		call: axios.delete<SaleResponse>(
			`${database}/sales/${id}`,
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};

export const addSaleItem = (
	database: string,
	id: number,
	item: ItemRequest,
) => {
	return {
		call: axios.post<SaleResponse>(
			`${database}/sales/${id}/line`,
			{
				product: item.product,
				quantity: item.quantity,
				unit_price: item.price,
				discount_amount: item.discount,
			},
			{ signal: controller.signal },
		),
		controller,
	};
};

export const updateSaleItem = (
	database: string,
	id: number,
	itemId: number,
	item: ItemRequest,
) => {
	return {
		call: axios.put<SaleResponse>(
			`${database}/sales/${id}/line/${itemId}`,
			{
				product: item.product,
				quantity: item.quantity,
				unit_price: item.price,
				discount_amount: item.discount,
			},
			{ signal: controller.signal },
		),
		controller,
	};
};

export const deleteSaleItem = (
	database: string,
	id: number,
	itemId: number,
) => {
	return {
		call: axios.delete<SaleResponse>(
			`${database}/sales/${id}/line/${itemId}`,
			{ signal: controller.signal },
		),
		controller,
	};
};
