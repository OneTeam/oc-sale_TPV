import { TableResponse } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

export const fetchTables = (database: string) => {
	const controller = loadAbort();
	return {
		call: axios.get<TableResponse[]>(`${database}/tables`, {
			signal: controller.signal,
		}),
		controller,
	};
};
