import { PartyResponse } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

export const fetchParties = (database: string) => {
	const controller = loadAbort();
	return {
		call: axios.get<PartyResponse[]>(`${database}/parties`, {
			signal: controller.signal,
		}),
		controller,
	};
};
