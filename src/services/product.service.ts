import { ProductResponse } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

export const fetchProducts = (database: string) => {
	const controller = loadAbort();
	return {
		call: axios.get<ProductResponse[]>(
			`${database}/products`,
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};
