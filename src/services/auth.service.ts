import { LoginRequest, LoginResponse, User } from '@/types';
import { loadAbort } from '@/utils';
import axios from 'axios';

export const login = (data: LoginRequest) => {
	const controller = loadAbort();
	return {
		call: axios.post<LoginResponse>(
			`/${data.database}/auth/login`,
			{
				username: data.username,
				password: data.password,
			},
			{
				signal: controller.signal,
			},
		),
		controller,
	};
};

export const me = (database: string) => {
	const controller = loadAbort();
	return {
		call: axios.get<User>(`${database}/auth/me`, {
			signal: controller.signal,
		}),
		controller,
	};
};
