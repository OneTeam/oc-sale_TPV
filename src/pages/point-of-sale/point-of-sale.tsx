/* eslint-disable react-hooks/exhaustive-deps */
import {
	categoriesAdapter,
	devicesAdapter,
	partiesAdapter,
	productsAdapter,
} from '@/adapters';
import { Loader } from '@/components';
import { useCallAndLoad } from '@/hooks';
import {
	setCategories,
	setDevices,
	setParties,
	setProducts,
} from '@/redux/slices/pos.slice';
import {
	fetchCategories,
	fetchParties,
	fetchProducts,
} from '@/services';
import {
	CategoryResponse,
	DeviceResponse,
	PartyResponse,
	ProductResponse,
} from '@/types';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Order, Sidebar, Storage } from './components';
import { AppStore } from '@/redux/store';
import { setError } from '@/redux/slices/error.slice';
import { useNavigate } from 'react-router-dom';
import { Routes } from '@/constants';
import { fetchDevices } from '@/services/devices.service';

const PointOfSale = () => {
	const { callEndpoint, loading } = useCallAndLoad();
	const { database } = useSelector((store: AppStore) => store.auth);

	const dispatch = useDispatch();
	const navigate = useNavigate();

	React.useEffect(() => {
		const getPos = async () => {
			try {
				const products = await callEndpoint(fetchProducts(database));
				const categories = await callEndpoint(fetchCategories(database));
				const parties = await callEndpoint(fetchParties(database));
				const devices = await callEndpoint(fetchDevices(database))

				dispatch(setProducts(productsAdapter(products.data as ProductResponse[])));
				dispatch(setCategories(categoriesAdapter(categories.data as CategoryResponse[])));
				dispatch(setParties(partiesAdapter(parties.data as PartyResponse[])));
				dispatch(setDevices(devicesAdapter(devices.data as DeviceResponse[])));

			} catch (error) {
				dispatch(
					setError({
						status: 500,
						message: 'Internal Server Error',
					}),
				);
				navigate(Routes.ERROR);
			}
		};

		getPos();
	}, [database, dispatch, navigate]);

	return loading ? (
		<Loader />
	) : (
		<main className='w-full h-screen max-h-screen bg-slate-50 dark:bg-slate-950 overflow-hidden scrollbar-hide transition-all duration-200'>
			<div className='w-full h-full flex'>
				<Sidebar />
				<Storage />
				<Order />
			</div>
		</main>
	);
};

export default PointOfSale;
