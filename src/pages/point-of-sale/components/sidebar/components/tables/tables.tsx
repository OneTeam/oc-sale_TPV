import { saleAdapter, salesAdapter, tablesAdapter } from '@/adapters';
import { useCallAndLoad } from '@/hooks';
import {
	setOrder,
	setOrders,
	setTables,
} from '@/redux/slices/sales.slice';
import { AppStore } from '@/redux/store';
import {
	changeTable,
	fetchSales,
	fetchTables,
	startSale,
} from '@/services';
import { Party, SaleResponse, Table, TableResponse } from '@/types';
import { formatCurrency } from '@/utils';
import { Dialog, Listbox, Tab, Transition } from '@headlessui/react';
import {
	CheckIcon,
	ChevronUpDownIcon,
} from '@heroicons/react/24/solid';
import React from 'react';
import { toast } from 'react-hot-toast';
import { useDispatch, useSelector } from 'react-redux';

export interface TablesProps {
	open: boolean;
	onClose: () => void;
}

export interface TablesState {
	matrix: number[][];
	table: Table | null;
	party: Party | null;
}

const Tables = ({ open, onClose }: TablesProps) => {
	const { callEndpoint, loading } = useCallAndLoad();
	const { device, orders, tables } = useSelector(
		(store: AppStore) => store.sales,
	);
	const { parties } = useSelector((store: AppStore) => store.pos);
	const { database, user } = useSelector((store: AppStore) => store.auth);

	const [matrix, setMatrix] = React.useState<TablesState['matrix']>(
		Array.from({ length: 6 }, () => Array(9).fill(0)),
	);
	const [selectedTable, setSelectedTable] =
		React.useState<TablesState['table']>(null);
	const [tableToChange, setTableToChange] =
		React.useState<TablesState['table']>(null);
	const [selectedParty, setSelectedParty] =
		React.useState<TablesState['party']>(null);

	const dispatch = useDispatch();

	const handlerClose = () => {
		setSelectedTable(null);
		setSelectedParty(null);
		setTableToChange(null);
		onClose();
	};

	const createSale = async () => {
		try {
			const order = await callEndpoint(
				startSale(database, {
					party: selectedParty?.id as number,
					table: selectedTable?.id as number,
				}),
			);
			const orders = await callEndpoint(
				fetchSales(database, device?.id as number),
			);
			const tablesResponse = await callEndpoint(
				fetchTables(database),
			);
			dispatch(
				setOrders(salesAdapter(orders.data as SaleResponse[])),
			);
			dispatch(setOrder(saleAdapter(order.data as SaleResponse)));
			dispatch(
				setTables(
					tablesAdapter(tablesResponse.data as TableResponse[]),
				),
			);
			handlerClose();
		} catch (error) {
			toast.error('Ocurrió un error al crear la venta');
		}
	};

	const handlerChangeTable = async (id: number) => {
		try {
			const order = await callEndpoint(
				changeTable(database, id, tableToChange?.id as number),
			);
			const orders = await callEndpoint(
				fetchSales(database, device?.id as number),
			);
			const tablesResponse = await callEndpoint(
				fetchTables(database),
			);
			dispatch(setOrder(saleAdapter(order.data as SaleResponse)));
			dispatch(
				setOrders(salesAdapter(orders.data as SaleResponse[])),
			);
			dispatch(
				setTables(
					tablesAdapter(tablesResponse.data as TableResponse[]),
				),
			);
			handlerClose();
		} catch (error) {
			toast.error('Ocurrió un error al cambiar de mesa');
		}
	};

	React.useEffect(() => {
		if (tables.length > 0) {
			const newMatrix = Array.from({ length: 6 }, () =>
				Array(9).fill(0),
			);
			tables.forEach((table) => {
				const { x, y } = table.position;
				newMatrix[x - 1][y - 1] = table.id;
			});
			setMatrix(newMatrix);
		}
	}, [tables]);

	return (
		<Transition appear show={open} as={React.Fragment}>
			<Dialog
				as='div'
				className='relative z-30'
				onClose={handlerClose}
			>
				<Transition.Child
					as={React.Fragment}
					enter='ease-out duration-300'
					enterFrom='opacity-0'
					enterTo='opacity-100'
					leave='ease-in duration-200'
					leaveFrom='opacity-100'
					leaveTo='opacity-0'
				>
					<div className='fixed inset-0 bg-black bg-opacity-40' />
				</Transition.Child>

				<div className='fixed inset-0 overflow-y-auto'>
					<div className='flex min-h-full h-screen items-center justify-center p-4 text-center'>
						<Transition.Child
							as={React.Fragment}
							enter='ease-out duration-300'
							enterFrom='opacity-0 scale-95'
							enterTo='opacity-100 scale-100'
							leave='ease-in duration-200'
							leaveFrom='opacity-100 scale-100'
							leaveTo='opacity-0 scale-95'
						>
							<Dialog.Panel className='w-full max-w-6xl h-[80%] overflow-hidden relative transform rounded-2xl bg-slate-50 dark:bg-slate-600 transition-all duration-200 p-2 text-left align-middle shadow-xl'>
								<div className='relative w-full h-full grid grid-cols-9 grid-rows-6 gap-2 bg-slate-300 dark:bg-slate-400 rounded-xl shadow-inner overflow-hidden p-2 transition-all duration-400'>
									{matrix.flat().map((item, index) => {
										const table = tables.find(
											(t) => t.id === item,
										);
										
										return table ? (
											<div
												key={index}
												onClick={() => {
													if(user?.device.id !== device?.id) {
														if (table.busy && orders.find( o => o.table === table.id)) {
															setOrder(orders.find( o => o.table === table.id))
															onClose()
														}
														return null
													}
													if(!table.busy || (table.busy && !!orders.find( order => order.table === table?.id))){
														setSelectedTable(table);
													}
												}}
												className={`w-full h-full ${
													table.busy && !orders.find( order => order.table === table?.id) ? 'bg-slate-400 dark:bg-slate-500' : 'bg-slate-200 dark:bg-slate-800'
												}  rounded-lg ${
													table.busy ? 'shadow-inner' : 'shadow'
												} relative flex flex-col items-center justify-center gap-2 p-2 transition-all duration-400 select-none`}
											>
												<span className='capitalize font-semibold text-2xl text-slate-700 dark:text-slate-200 transition-all duration-200'>
													{table.name}
												</span>
												<div
													className={`absolute bottom-2 right-2 w-3 ${
														table.busy
															? 'bg-red-600'
															: 'bg-green-600 animate-pulse'
													} aspect-square rounded-full select-none`}
												/>
												{table.busy && orders.find( order => order.table === table?.id) && (
													<span className='absolute bottom-2 left-2 text-xs text-slate-600 dark:text-slate-300'>
														{formatCurrency(
															orders.find(
																(order) => order.table === table.id,
															)?.total as number,
														)}
													</span>
												)}
											</div>
										) : (
											<div
												key={index}
												className='w-full h-full bg-transparent'
											></div>
										);
									})}
									<div
										id='table-background'
										onClick={(e) => {
											const target = e.target as HTMLDivElement;
											if (target.id === 'table-background') {
												setSelectedParty(null);
												setSelectedTable(null);
											}
										}}
										className={`absolute top-0 left-0 w-full h-full bg-black/40 ${
											!selectedTable
												? 'opacity-0 invisible'
												: 'opacity-100 visible'
										} flex items-center justify-center transition-all duration-200`}
									>
										<div className='w-full max-w-md p-2 transition-all duration-200'>
											<div
												className={`w-full rounded-md bg-slate-50 dark:bg-slate-600 ${
													!selectedTable
														? 'translate-x-full opacity-0 invisible delay-150'
														: 'translate-x-0 opacity-100 visible'
												} transition-all duration-200 p-4 select-none flex flex-col justify-between`}
											>
												<div className='w-full mb-6'>
													<h2 className='text-4xl font-bold text-slate-900 dark:text-slate-100 transition-all duration-200'>
														Mesa {selectedTable?.name}
													</h2>
													{selectedTable?.busy && (
														<h3 className='text-lg text-slate-700 dark:text-slate-300 transition-all duration-200'>
															Venta{' '}
															{
																orders.find(
																	(order) =>
																		order.table === selectedTable?.id,
																)?.id
															}
														</h3>
													)}
												</div>
												{!selectedTable?.busy && (
													<div className='w-full'>
														<Listbox
															value={selectedParty}
															onChange={setSelectedParty}
														>
															<div className='w-full relative mt-1 z-[1] border dark:border-none rounded-lg'>
																<Listbox.Button className='relative w-full border text-slate-900 dark:text-slate-800 border-slate-50 dark:border-slate-400 cursor-default rounded-lg bg-slate-50 dark:bg-slate-400 transition-all duration-200 py-2 pl-3 pr-10 text-left shadow focus:outline-none focus-visible:border-none focus-visible:ring-2 focus-visible:ring-none focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-none sm:text-sm'>
																	<span className='block truncate capitalize'>
																		{selectedParty?.name ||
																			'Selecciona un cliente'}
																	</span>
																	<span className='pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2'>
																		<ChevronUpDownIcon
																			className='h-5 w-5 text-slate-400 dark:text-slate-800 transition-all duration-200'
																			aria-hidden='true'
																		/>
																	</span>
																</Listbox.Button>
																<Transition
																	as={React.Fragment}
																	leave='transition ease-in duration-100'
																	leaveFrom='opacity-100'
																	leaveTo='opacity-0'
																>
																	<Listbox.Options className='absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-slate-50 dark:bg-slate-400 py-1 text-base shadow-lg ring-1 ring-slate-950 transition-all duration-200 ring-opacity-5 focus:outline-none sm:text-sm'>
																		{parties.map((person) => (
																			<Listbox.Option
																				key={person.id}
																				className={({ active }) =>
																					`relative cursor-default select-none py-2 pl-10 pr-4 ${
																						active
																							? 'bg-slate-100 text-slate-900 dark:bg-slate-300 font-medium dark:text-slate-950 transition-all duration-200'
																							: 'text-slate-900'
																					}`
																				}
																				value={person}
																			>
																				{({ selected }) => (
																					<>
																						<span
																							className={`block truncate capitalize ${
																								selected
																									? 'font-medium'
																									: 'font-normal'
																							}`}
																						>
																							{person.name}
																						</span>
																						{selected ? (
																							<span className='absolute inset-y-0 left-0 flex items-center pl-3 text-slate-600  transition-all duration-200'>
																								<CheckIcon
																									className='h-5 w-5'
																									aria-hidden='true'
																								/>
																							</span>
																						) : null}
																					</>
																				)}
																			</Listbox.Option>
																		))}
																	</Listbox.Options>
																</Transition>
															</div>
														</Listbox>
														<button
															disabled={loading}
															onClick={async () => await createSale()}
															className={`w-full flex items-center justify-center py-5 mt-4 ${
																selectedParty
																	? 'bg-slate-600 text-slate-100 dark:bg-slate-800 transition-all duration-200'
																	: 'bg-slate-200 text-slate-300 dark:bg-slate-700 transition-all duration-200'
															} rounded-lg`}
														>
															{loading ? (
																<>
																	{' '}
																	<span className='flex w-5 h-5 items-center justify-center border-4 border-white border-l-transparent rounded-full mr-4 animate-spin' />
																	<span>Iniciando...</span>
																</>
															) : (
																<span className='text-xl'>
																	Iniciar venta
																</span>
															)}
														</button>
													</div>
												)}
												{selectedTable?.busy && (
													<div className='w-full transition-all duration-200'>
														<Tab.Group>
															<Tab.List className='flex space-x-1 rounded-xl'>
																<Tab
																	className={({ selected }) =>
																		`${
																			selected
																				? 'bg-slate-200 dark:bg-slate-400 text-slate-600 dark:text-slate-800'
																				: 'text-slate-900 dark:text-slate-300'
																		} w-full py-2.5 text-sm leading-5 font-medium rounded-t-lg`
																	}
																>
																	Resumen
																</Tab>
																<Tab
																	className={({ selected }) =>
																		`${
																			selected
																				? 'bg-slate-200 dark:bg-slate-400 text-slate-600 dark:text-slate-800'
																				: 'text-slate-900 dark:text-slate-300'
																		} w-full py-2.5 text-sm leading-5 font-medium rounded-t-lg`
																	}
																>
																	Cambiar mesa
																</Tab>
															</Tab.List>
															<Tab.Panels>
																<Tab.Panel className='h-36 flex flex-col justify-end bg-slate-200 dark:bg-slate-400 transition-all duration-200 rounded-xl rounded-tl-none p-4'>
																	<h3 className='text-xl pt-1 mb-2 font-bold w-full flex justify-between text-salte-950 dark:text-slate-100 transition-all duration-200'>
																		Total{' '}
																		<span className='font-black'>
																			{formatCurrency(
																				orders.find(
																					(order) =>
																						order.table ===
																						selectedTable?.id,
																				)?.total as number,
																			)}
																		</span>
																	</h3>
																	<button
																		onClick={() => {
																			dispatch(
																				setOrder(
																					orders.find(
																						(order) =>
																							order.table ===
																							selectedTable?.id,
																					),
																				),
																			);
																			handlerClose();
																		}}
																		className='w-full py-3 flex items-center justify-center bg-slate-50 dark:bg-slate-300 text-slate-900 dark:text-slate-700 shadow rounded-lg'
																	>
																		<span className='text-slate-900 dark:text-slate-700 transition-all duration-200 font-semibold'>
																			Ir a la venta
																		</span>
																	</button>
																</Tab.Panel>
																<Tab.Panel className='h-36 flex flex-col justify-end bg-slate-200 dark:bg-slate-400 transition-all duration-200 rounded-xl rounded-tr-none p-4'>
																	<Listbox
																		value={tableToChange}
																		onChange={setTableToChange}
																	>
																		<div className='w-full relative mb-2 border dark:border-none rounded-lg'>
																			<Listbox.Button className='relative w-full border text-slate-900 dark:text-slate-800 border-slate-50 dark:border-slate-400 cursor-default rounded-lg bg-slate-50 dark:bg-slate-400 transition-all duration-200 py-2 pl-3 pr-10 text-left shadow focus:outline-none focus-visible:border-none focus-visible:ring-2 focus-visible:ring-none focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-none sm:text-sm'>
																				<span className='block truncate capitalize'>
																					{tableToChange?.name ||
																						'Selecciona una mesa'}
																				</span>
																				<span className='pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2'>
																					<ChevronUpDownIcon
																						className='h-5 w-5 text-slate-400 dark:text-slate-800 transition-all duration-200'
																						aria-hidden='true'
																					/>
																				</span>
																			</Listbox.Button>
																			<Transition
																				as={React.Fragment}
																				leave='transition ease-in duration-100'
																				leaveFrom='opacity-100'
																				leaveTo='opacity-0'
																			>
																				<Listbox.Options className='absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-slate-50 dark:bg-slate-400 py-1 text-base shadow-lg ring-1 ring-slate-950 transition-all duration-200 ring-opacity-5 focus:outline-none sm:text-sm'>
																					{tables
																						.filter(
																							(table) => !table.busy,
																						)
																						.map((person) => (
																							<Listbox.Option
																								key={person.id}
																								className={({
																									active,
																								}) =>
																									`relative cursor-default select-none py-2 pl-10 pr-4 ${
																										active
																											? 'bg-slate-100 text-slate-900 dark:bg-slate-300 font-medium dark:text-slate-950 transition-all duration-200'
																											: 'text-slate-900'
																									}`
																								}
																								value={person}
																							>
																								{({ selected }) => (
																									<>
																										<span
																											className={`block truncate capitalize ${
																												selected
																													? 'font-medium'
																													: 'font-normal'
																											}`}
																										>
																											{person.name}
																										</span>
																										{selected ? (
																											<span className='absolute inset-y-0 left-0 flex items-center pl-3 text-slate-600  transition-all duration-200'>
																												<CheckIcon
																													className='h-5 w-5'
																													aria-hidden='true'
																												/>
																											</span>
																										) : null}
																									</>
																								)}
																							</Listbox.Option>
																						))}
																				</Listbox.Options>
																			</Transition>
																		</div>
																	</Listbox>
																	<button
																		disabled={
																			!tableToChange || loading
																		}
																		onClick={() => {
																			handlerChangeTable(
																				orders.find(
																					(order) =>
																						order.table ===
																						selectedTable?.id,
																				)?.id as number,
																			);
																		}}
																		className='w-full py-3 flex items-center justify-center bg-slate-50 dark:bg-slate-300 text-slate-900 dark:text-slate-700 shadow rounded-lg'
																	>
																		{loading ? (
																			<>
																				<span className='flex w-5 h-5 items-center justify-center border-4 border-slate-900 dark:border-slate-700 transfer border-l-transparent dark:border-t-slate-500 rounded-full mr-4 animate-spin' />
																				<span>Cambiando...</span>
																			</>
																		) : (
																			<span className='text-slate-900 dark:text-slate-700 transition-all duration-200 font-semibold'>
																				Cambiar de mesa
																			</span>
																		)}
																	</button>
																</Tab.Panel>
															</Tab.Panels>
														</Tab.Group>
													</div>
												)}
											</div>
										</div>
									</div>
								</div>
							</Dialog.Panel>
						</Transition.Child>
					</div>
				</div>
			</Dialog>
		</Transition>
	);
};

export default Tables;
