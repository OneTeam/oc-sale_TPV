import { setOrder } from '@/redux/slices/sales.slice';
import { AppStore } from '@/redux/store';
import { Dialog, Transition } from '@headlessui/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

export interface OrdersProps {
	open: boolean;
	onClose: () => void;
}

const Orders = ({ open, onClose }: OrdersProps) => {
	const { order, orders } = useSelector(
		(store: AppStore) => store.sales,
	);

	const dispatch = useDispatch();

	return (
		<Transition appear show={open} as={React.Fragment}>
			<Dialog as='div' className='relative z-30' onClose={onClose}>
				<Transition.Child
					as={React.Fragment}
					enter='ease-out duration-300'
					enterFrom='opacity-0'
					enterTo='opacity-100'
					leave='ease-in duration-200'
					leaveFrom='opacity-100'
					leaveTo='opacity-0'
				>
					<div className='fixed inset-0 bg-black bg-opacity-40' />
				</Transition.Child>
				<div className='fixed inset-0 overflow-y-auto'>
					<div className='flex min-h-full items-center justify-center p-4 text-center'>
						<Transition.Child
							as={React.Fragment}
							enter='ease-out duration-300'
							enterFrom='opacity-0 scale-95'
							enterTo='opacity-100 scale-100'
							leave='ease-in duration-200'
							leaveFrom='opacity-100 scale-100'
							leaveTo='opacity-0 scale-95'
						>
							<Dialog.Panel className='w-full max-w-lg transform rounded-2xl bg-slate-50 dark:bg-slate-600 transition-all duration-200 p-6 text-left align-middle shadow-xl'>
							<Dialog.Title
									as='h3'
									className='text-lg font-medium leading-6 text-gray-900 dark:text-slate-100 transition-all duration-200'
								>
									Selecciona una venta
								</Dialog.Title>
								<div className='w-full grid grid-cols-3 gap-2 mt-4'>
									{orders?.map((sale) => (
										<button
											key={sale.id}
											onClick={() => {
												dispatch(setOrder(sale));
												onClose();
											}}
											className={`w-full h-16 ${
												order?.id === sale.id
													? 'bg-slate-600 text-slate-300 dark:bg-slate-400 dark:text-slate-700'
													: 'bg-slate-50 text-slate-900 dark:bg-slate-700 dark:text-slate-200'
											} rounded-xl shadow flex items-center justify-center px-4 overflow-hidden text-center transition-all duration-200 border dark:border-none`}
										>
											<h4 className='font-medium capitalize'>venta {sale.id}</h4>
										</button>
									))}
								</div>
							</Dialog.Panel>
						</Transition.Child>
					</div>
				</div>
			</Dialog>
		</Transition>
	);
};

export default Orders;
