import { AppStore } from '@/redux/store';
import { useSelector } from 'react-redux';

export interface DeviceProps {
	openModal: () => void;
}

const Device = ({ openModal }: DeviceProps) => {
	const { device } = useSelector((store: AppStore) => store.sales);

	return (
		<div className='w-full'>
			<button
				onClick={() => {
					openModal();
				}}
				className='w-full py-2 bg-slate-50 dark:bg-slate-600  transition-all duration-200 rounded-lg shadow flex items-center justify-center'
			>
				<span className='capitalize font-semibold text-slate-700 dark:text-slate-200 transition-all duration-200'>
					{device?.name}
				</span>
			</button>
		</div>
	);
};

export default Device;
