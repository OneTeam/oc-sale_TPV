/* eslint-disable react-hooks/exhaustive-deps */
import { salesAdapter, tablesAdapter } from '@/adapters';
import { useCallAndLoad } from '@/hooks';
import {
	setOrder,
	setOrders,
	setTables,
} from '@/redux/slices/sales.slice';
import { AppStore } from '@/redux/store';
import { fetchSales, fetchTables } from '@/services';
import { SaleResponse, TableResponse } from '@/types';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
	Device,
	Devices,
	Order,
	Orders,
	Tables,
	User,
} from './components';
import { PlusSmallIcon } from '@heroicons/react/24/solid';
import { toast } from 'react-hot-toast';
import { Roles } from '@/constants';

export interface SidebarState {
	open: boolean;
	openDeviceModal: boolean;
	openOrderModal: boolean;
	openSalesModal: boolean;
	openTablesModal: boolean;
}

const Sidebar = () => {
	const { callEndpoint } = useCallAndLoad();
	const { user, database } = useSelector(
		(store: AppStore) => store.auth,
	);
	const { device, tables, orders } = useSelector(
		(store: AppStore) => store.sales,
	);

	const [openDeviceModal, setOpenDeviceModal] =
		React.useState<SidebarState['openDeviceModal']>(false);
	const [openOrderModal, setOpenOrderModal] =
		React.useState<SidebarState['openOrderModal']>(false);
	const [openSalesModal, setOpenSalesModal] =
		React.useState<SidebarState['openSalesModal']>(false);
	const [openTablesModal, setOpenTablesModal] =
		React.useState<SidebarState['openTablesModal']>(false);

	const dispatch = useDispatch();

	React.useEffect(() => {
		const getSales = async () => {
			try {
				const response = await callEndpoint(
					fetchSales(database, device?.id as number),
				);
				const sales = salesAdapter(response.data as SaleResponse[]);
				dispatch(setOrders(sales));
				dispatch(setOrder(sales[0]));
			} catch (error) {
				toast.error('Ocurrió un error al obtener las ventas');
			}
		};

		const getTables = async () => {
			try {
				const response = await callEndpoint(fetchTables(database));
				dispatch(
					setTables(tablesAdapter(response.data as TableResponse[])),
				);
			} catch (error) {
				toast.error('Ocurrió un error al obtener las mesas');
			}
		};
		getTables();
		getSales();
	}, [device, dispatch, user]);

	return (
		<>
			<Devices
				open={openDeviceModal}
				onClose={() => setOpenDeviceModal(false)}
			/>
			<Order
				open={openOrderModal}
				onClose={() => setOpenOrderModal(false)}
			/>
			<Orders
				open={openSalesModal}
				onClose={() => setOpenSalesModal(false)}
			/>
			<Tables
				open={openTablesModal}
				onClose={() => setOpenTablesModal(false)}
			/>
			<aside className='fixed top-0 left-0 z-20 h-full w-64 p-4 select-none'>
				<div className='relative w-full h-full bg-slate-200 dark:bg-slate-800 rounded-2xl shadow-inner flex flex-col gap-2 p-2 transition-all duration-200'>
					{user?.role === Roles.ADMIN && (
						<Device openModal={() => setOpenDeviceModal(true)} />
					)}
					{tables.length > 0 && (
						<button
							onClick={() => setOpenTablesModal(true)}
							className='w-full py-2 bg-slate-50 dark:bg-slate-600  transition-all duration-200 rounded-lg shadow flex items-center justify-center'
						>
							<span className='capitalize font-semibold text-slate-700 dark:text-slate-200 transition-all duration-200'>
								Mesas
							</span>
						</button>
					)}
					<button
						onClick={() => {
							if (orders.length > 0) {
								setOpenSalesModal(true);
							}
						}}
						className='w-full py-2 bg-slate-50 dark:bg-slate-600  transition-all duration-200 rounded-lg shadow flex items-center justify-center'
					>
						<span className='capitalize font-semibold text-slate-700 dark:text-slate-200 transition-all duration-200'>
							Ventas{' '}
							<span className='text-xs text-slate-400'>
								({orders.length})
							</span>
						</span>
					</button>
					{user?.device.id === device?.id && (
						<button
							onClick={() => setOpenOrderModal(true)}
							className='w-full h-10  bg-slate-300 text-slate-500 dark:bg-slate-700 dark:text-slate-400 transition-all duration-200 rounded-lg flex items-center justify-center gap-2'
						>
							<PlusSmallIcon width={20} />
							<span className='capitalize transition-all duration-200'>
								Nueva venta
							</span>
						</button>
					)}
					<User />
				</div>
			</aside>
		</>
	);
};

export default Sidebar;
