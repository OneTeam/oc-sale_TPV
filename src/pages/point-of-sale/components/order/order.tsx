import { salesAdapter, tablesAdapter } from '@/adapters';
import { useCallAndLoad } from '@/hooks';
import {
	setOrder,
	setOrders,
	setTables,
} from '@/redux/slices/sales.slice';
import { AppStore } from '@/redux/store';
import {
	canceledSale,
	completeSaleSuccess,
	fetchSales,
	fetchTables,
	printSale,
} from '@/services';
import { PrintResponse, Sale, SaleResponse, TableResponse } from '@/types';
import { formatCurrency, printTicket } from '@/utils';
import {
	BanknotesIcon,
	QrCodeIcon,
	TrashIcon,
} from '@heroicons/react/24/solid';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Item, Product } from './components';
import { toast } from 'react-hot-toast';

import empty from '@/assets/empty.svg';

export interface OrderState {
	sale: Sale | null;
	payment: 'cash' | 'transfer';
	action: 'complete' | 'delete' | 'print' | undefined;
}

const Order = () => {
	const { callEndpoint, loading } = useCallAndLoad();
	const { order, device, tables } = useSelector(
		(store: AppStore) => store.sales,
	);
	const { parties, products } = useSelector(
		(store: AppStore) => store.pos,
	);
	const { database, user } = useSelector(
		(store: AppStore) => store.auth,
	);

	const [paymentMethod, setPaymentMethod] =
		React.useState<OrderState['payment']>('cash');
	const [action, setAction] =
		React.useState<OrderState['action']>(undefined);

	const dispatch = useDispatch();

	const handlerCompleteSale = async () => {
		try {
			setAction('complete');
			const response = await callEndpoint(
				completeSaleSuccess(
					database,
					order?.id as number,
					paymentMethod,
				),
			);
			if (response) {
				printTicket(response as PrintResponse, order?.id as number);
				toast.success(`Venta ${order?.id}`);
				const orders = await callEndpoint(
					fetchSales(database, device?.id as number),
				);
				const result = salesAdapter(orders.data as SaleResponse[]);
				dispatch(setOrders(result));
				dispatch(setOrder(result[0]));
				if (order?.table !== null) {
					const tables = await callEndpoint(fetchTables(database));
					dispatch(
						setTables(tablesAdapter(tables.data as TableResponse[])),
					);
				}
			}
		} catch (error) {
			toast.error('No se pudo completar la venta, intente de nuevo');
		} finally {
			setPaymentMethod('cash');
			setAction(undefined);
		}
	};

	const handlerCancelSale = async () => {
		try {
			setAction('delete');
			const response = await callEndpoint(
				canceledSale(database, order?.id as number),
			);
			if (response) {
				toast.success(`Venta ${order?.id} cancelada`);
				const orders = await callEndpoint(
					fetchSales(database, device?.id as number),
				);
				const result = salesAdapter(orders.data as SaleResponse[]);
				dispatch(setOrders(result));
				dispatch(setOrder(result[0]));
				if (order?.table !== 0) {
					const tables = await callEndpoint(fetchTables(database));
					dispatch(
						setTables(tablesAdapter(tables.data as TableResponse[])),
					);
				}
			}
		} catch (error) {
			toast.error('No se pudo cancelar la venta, intente de nuevo');
		} finally {
			setPaymentMethod('cash');
			setAction(undefined);
		}
	};

	const handlerPrintSale = async () => {
		try {
			setAction('print');
			const response = await callEndpoint(
				printSale(
					database,
					order?.id as number,
				),
			);
			if (response) {
				printTicket(response as PrintResponse, order?.id as number);
				toast.success(`Venta ${order?.id} impresa`);
			}
		} catch (error) {
			toast.error('No se pudo imprimir el ticket, intente de nuevo');
		} finally {
			setAction(undefined);
		}
	};

	return (
		<>
			<Product />
			<aside className='fixed top-0 right-0 z-10 h-full w-[576px] p-4'>
				<div className='w-full h-full bg-slate-200 dark:bg-slate-900 transition-all duration-200 rounded-2xl shadow-inner p-2 flex items-center justify-center'>
					{order ? (
						<div className='relative w-full h-full flex flex-col'>
							<div className='absolute top-0 left-0 w-full flex items-center bg-slate-50 dark:bg-slate-500 transition-all duration-200 p-2 shadow rounded-xl'>
								<div className='w-full relative flex flex-col items-start justify-center'>
									<h2 className='text-3xl text-slate-900 dark:text-slate-100 transition-all duration-200 font-bold'>
										Venta{' '}
										<span className='text-slate-800 dark:text-slate-300 transition-all duration-200'>
											{order.id}
										</span>
									</h2>
									{order.table !== 0 && (
										<p className='text-slate-800 dark:text-slate-200 transition-all duration-200 text-xl capitalize pl-1'>
											<span className='font-medium'>Mesa:</span>{' '}
											<span className='italic'>
												{
													tables.find(
														(table) => table.id == order.table,
													)?.name
												}
											</span>
										</p>
									)}
									<p className='text-slate-600 dark:text-slate-300 transition-all duration-200 text-xs capitalize pl-1'>
										<span className='font-medium'>Cliente:</span>{' '}
										<span className='italic'>
											{
												parties.find(
													(party) => party.id == order.party,
												)?.name
											}
										</span>
									</p>
								</div>
								{user?.device.id === device?.id && (
									<button
										disabled={loading}
										onClick={async () => await handlerCancelSale()}
										className='text-red-600 dark:text-slate-900 transition-all duration-200 w-20 aspect-square flex items-center justify-center'
									>
										{loading && action === 'delete' ? (
											<div className='w-8 aspect-square border-4 border-red-600 dark:border-slate-900 border-t-white dark:border-t-slate-600 rounded-full animate-spin transition-all duration-200' />
										) : (
											<TrashIcon width={32} />
										)}
									</button>
								)}
							</div>
							<div
								className={`w-full h-full mt-24 ${user?.device.id === device?.id ? 'mb-64' : 'mb-24'
									}  pb-2 overflow-y-auto scrollbar-hide transition-all duration-200`}
							>
								{order?.items?.map((item) => (
									<Item key={item.id} item={item} loading={loading} />
								))}
							</div>
							<div className='absolute bottom-0 left-0 w-full flex flex-col gap-2 p-2 bg-slate-300 dark:bg-slate-700 transition-all duration-200 rounded-xl shadow'>
								<div className='divide-y divide-dashed divide-slate-400 dark:divide-slate-500 transition-all duration-200'>
									<div className='pb-1'>
										<p className='text-xs w-full flex justify-between text-slate-500 dark:text-slate-400 transition-all duration-200'>
											Subtotal:{' '}
											<span className='font-medium'>
												{formatCurrency(
													order.items
														.map(
															(item) =>
																item.quantity *
																(products.find(
																	(product) =>
																		product.id === item.product,
																)?.price || 0),
														)
														.reduce((sum, value) => sum + value, 0),
												)}
											</span>
										</p>
										<p className='text-xs w-full flex justify-between text-slate-500 dark:text-slate-400 transition-all duration-200'>
											Descuento:{' '}
											<span className='font-medium'>
												{formatCurrency(
													order.items
														.map(
															(item) =>
																item.quantity *
																(products.find(
																	(product) =>
																		product.id === item.product,
																)?.price || 0) -
																item.price,
														)
														.reduce((sum, value) => sum + value, 0),
												)}
											</span>
										</p>
									</div>
									<h3 className='text-2xl pt-1 font-bold w-full flex justify-between text-salte-950 dark:text-slate-100 transition-all duration-200'>
										Total{' '}
										<span className='font-black'>
											{formatCurrency(order.total)}
										</span>
									</h3>
								</div>
								{user?.device.id === device?.id && (
									<div className='w-full flex flex-col gap-2'>
										<div className='w-full'>
											<h4 className='text-sm text-slate-500 dark:text-slate-300 transition-all duration-200 mb-1 font-semibold'>
												Método de pago
											</h4>
											<div className='w-full flex gap-2'>
												<button
													disabled={loading}
													onClick={() => setPaymentMethod('cash')}
													className={`flex-1 h-14 border-2 border-slate-50 dark:border-slate-300 shadow rounded-lg flex flex-col items-center justify-center transition-all duration-200 ${paymentMethod === 'cash'
															? 'bg-slate-50 text-slate-900 dark:bg-slate-300 transition-all duration-200'
															: 'text-slate-500 dark:text-slate-900 transition-all duration-200'
														} transition-all duration-200`}
												>
													<BanknotesIcon width={24} />
													<span className='text-slate-900 dark:text-slate-700 transition-all duration-200 font-semibold'>
														Efectivo
													</span>
												</button>
												<button
													disabled={loading}
													onClick={() => setPaymentMethod('transfer')}
													className={`flex-1 h-14 border-2 border-slate-50 dark:border-slate-300 shadow rounded-lg flex flex-col items-center justify-center transition-all duration-200 ${paymentMethod === 'transfer'
															? 'bg-slate-50 text-slate-900 dark:bg-slate-300 transition-all duration-200'
															: 'text-slate-500 dark:text-slate-900 transition-all duration-200'
														} transition-all duration-200`}
												>
													<QrCodeIcon width={24} />
													<span className='text-slate-900 dark:text-slate-700 transition-all duration-200 font-semibold'>
														Transferencia
													</span>
												</button>
											</div>
										</div>
										<div className='w-full flex gap-2'>
											<button
												disabled={loading}
												onClick={async () =>
													await handlerCompleteSale()
												}
												className='h-14 flex-1 items-center justify-center bg-slate-50 dark:bg-slate-300 text-slate-900 dark:text-slate-700 shadow rounded-lg transition-all duration-200 font-semibold'
											>
												{loading && action === 'complete' ? (
													<>
														<span className='flex w-5 h-5 items-center justify-center border-4 border-slate-900 dark:border-slate-700 transfer border-l-transparent dark:border-t-slate-500 rounded-full mr-4 animate-spin' />
														<span>Pagando...</span>
													</>
												) : (
													<span className='text-slate-900 dark:text-slate-700 transition-all duration-200 font-semibold'>
														Pagar
													</span>
												)}
											</button>
											<button
												disabled={loading}
												onClick={async () =>
													await handlerPrintSale()
												}
												className='h-14 flex-1 items-center justify-center bg-slate-50 dark:bg-slate-300 text-slate-900 dark:text-slate-700 shadow rounded-lg transition-all duration-200 font-semibold'
											>
												{loading && action === 'print' ? (
													<>
														<span className='flex w-5 h-5 items-center justify-center border-4 border-slate-900 dark:border-slate-700 transfer border-l-transparent dark:border-t-slate-500 rounded-full mr-4 animate-spin' />
														<span>Imprimiendo...</span>
													</>
												) : (
													<span className='text-slate-900 dark:text-slate-700 transition-all duration-200 font-semibold'>
														Imprimir
													</span>
												)}
											</button>
										</div>
									</div>
								)}
							</div>
						</div>
					) : (
						<div className='w-3/4 flex flex-col items-center justify-center'>
							<figure className='w-3/4'>
								<img
									src={empty}
									alt='empty'
									className='w-full h-auto mx-auto'
								/>
							</figure>
							<h4 className='mt-10 text-2xl text-center leading-none font-bold text-slate-950 dark:text-slate-100 transition-all duration-200'>
								Selecciona o inicia una nueva venta
							</h4>
						</div>
					)}
				</div>
			</aside>
		</>
	);
};

export default Order;
