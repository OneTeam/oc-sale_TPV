import { openModal } from '@/redux/slices/modal.slice';
import { AppStore } from '@/redux/store';
import { Item as ItemType } from '@/types';
import { formatCurrency } from '@/utils';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

export interface ItemProps {
	item: ItemType;
	loading: boolean;
}

const Item: React.FC<ItemProps> = ({ item, loading }) => {
	const { products } = useSelector((store: AppStore) => store.pos);
	const { device } = useSelector((store: AppStore) => store.sales);
	const { user } = useSelector((store: AppStore) => store.auth);
	const dispatch = useDispatch();

	return (
		<button
			disabled={loading}
			onClick={() => {
				if (user?.device.id === device?.id) {
					dispatch(
						openModal({
							type: 'edit',
							id: item.product,
							item: item,
						}),
					);
				}
			}}
			className='w-full h-14 bg-slate-300 dark:bg-slate-700 transition-all duration-200 rounded-xl shadow flex flex-col items-center px-4 my-2 select-none'
		>
			<div className='w-full h-5 flex items-center'>
				<span className='w-4/6 text-xs text-left text-slate-600 dark:text-slate-300 transition-all duration-200 capitalize'>
					Producto
				</span>
				<span className='w-1/6 text-xs text-center text-slate-600 dark:text-slate-300 transition-all duration-200'>
					Inventario
				</span>
				<span className='w-1/6 text-xs text-center text-slate-600 dark:text-slate-300 transition-all duration-200'>
					Cantidad
				</span>
				<span className='w-1/6 text-xs text-right text-slate-600 dark:text-slate-300 transition-all duration-200'>
					Precio
				</span>
			</div>
			<div className='w-full flex items-center'>
				<span className='w-4/6 text-sm capitalize font-medium text-left text-slate-950 dark:text-slate-300 transition-all duration-200'>
					{products.find((product) => product.id == item.product)?.name}
				</span>
				<span className='w-1/6 text-xs text-center text-slate-950 dark:text-slate-300 transition-all duration-200'>
					{item.stock}
				</span>
				<span className='w-1/6 text-xs text-center text-slate-950 dark:text-slate-300 transition-all duration-200'>
					{item.quantity}
				</span>
				<span className='w-1/6 text-xs text-slate-700 dark:text-slate-200 transition-all duration-200 font-semibold text-right'>
					{formatCurrency(item.price)}
				</span>
			</div>
		</button>
	);
};

export default Item;
