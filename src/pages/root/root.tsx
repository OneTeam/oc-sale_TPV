/* eslint-disable react-hooks/exhaustive-deps */
import { userAdapter } from '@/adapters';
import { Loader } from '@/components';
import { Routes } from '@/constants';
import { useCallAndLoad } from '@/hooks';
import { setDatabase, setUser } from '@/redux/slices/auth.slice';
import { setDevice } from '@/redux/slices/sales.slice';
import { AppStore } from '@/redux/store';
import { me } from '@/services';
import { UserResponse } from '@/types';
import axios from 'axios';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

const Root = () => {
	const { callEndpoint } = useCallAndLoad();
	const { isAuthenticated } = useSelector(
		(store: AppStore) => store.auth,
	);

	const dispatch = useDispatch();
	const navigate = useNavigate();

	React.useEffect(() => {
		const session = async () => {
			if (isAuthenticated) {
				navigate(Routes.POINT_OF_SALE);
			} else {
				const TOKEN = sessionStorage.getItem('token');
				const DATABASE = sessionStorage.getItem('database');
				if (TOKEN) {
					try {
						axios.defaults.headers.common['Authorization'] = `Bearer ${TOKEN}`
						const response = await callEndpoint(
							me(DATABASE as string),
						);
						const user = userAdapter(response.data as UserResponse)
						dispatch(setDatabase(DATABASE));
						dispatch(setUser(user));
						dispatch(setDevice(user.device))
						navigate(Routes.POINT_OF_SALE);
					} catch (error) {
						navigate(Routes.LOGIN);
					}
				} else {
					navigate(Routes.LOGIN);
				}
			}
		};

		session();
	}, []);

	return <Loader />;
};

export default Root;
