import { Loader } from '@/components';
import { Routes } from '@/constants';
import { resetError } from '@/redux/slices/error.slice';
import { AppStore } from '@/redux/store';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import serverDown from '@/assets/server-down.svg';
import { resetAuth } from '@/redux/slices/auth.slice';

export interface ServerErrorState {
	busy: boolean;
}

const ServerError = () => {
	const error = useSelector((store: AppStore) => store.error);

	const [busy, setBusy] =
		React.useState<ServerErrorState['busy']>(false);

	const dispatch = useDispatch();
	const navigate = useNavigate();

	const handlerReload = () => {
		dispatch(resetError());
		dispatch(resetAuth())
		window.sessionStorage.removeItem('token')
		window.sessionStorage.removeItem('database')
		window.location.href = Routes.ROOT
	};

	React.useEffect(() => {
		if (error.status === 0) {
			navigate(Routes.ROOT);
		} else {
			setBusy(false);
		}
	}, [error, navigate]);

	return busy ? (
		<Loader />
	) : (
		<div className='w-full h-screen flex flex-col items-center justify-center bg-slate-50 dark:bg-slate-950'>
			<figure className='w-1/3 mb-10'>
				<img src={serverDown} alt='Server Down' />
			</figure>
			<h2 className='text-slate-900 dark:text-slate-100 text-5xl font-extrabold mb-8'>
				{error.message || 'Internal Server Error'}
			</h2>
			<button
				onClick={handlerReload}
				className='bg-slate-600 text-slate-100 transition-all duration-200 px-24 py-4 text-xl shadow rounded-xl'
			>
				Recargar
			</button>
		</div>
	);
};

export default ServerError;
