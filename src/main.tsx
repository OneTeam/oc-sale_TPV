import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import App from './app';
import './index.css';
import store from './redux/store';
import axios from 'axios';

const BASE_URL = import.meta.env.VITE_API_URL;
axios.defaults.baseURL = BASE_URL;

ReactDOM.createRoot(
	document.getElementById('root') as HTMLElement,
).render(
	<React.StrictMode>
		<Provider store={store}>
			<App />
		</Provider>
	</React.StrictMode>,
);
