import { PrintResponse } from '@/types';

export const printTicket = (response: PrintResponse, orderId: number) =>{
    const blob = new Blob([response.data as BlobPart], { type: 'application/pdf' });
    const url = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = url;
    link.setAttribute('download', `Ticket Venta-${orderId}.pdf`);
    document.body.appendChild(link);
    link.click();
    URL.revokeObjectURL(url);
    document.body.removeChild(link);
} 