import { configureStore } from '@reduxjs/toolkit';

import posReducer, { PosState } from './slices/pos.slice';
import modalReducer, { ModalState } from './slices/modal.slice';
import authReducer, { AuthState } from './slices/auth.slice';
import salesReducer, { SalesState } from './slices/sales.slice';
import errorReducer, { ErrorState } from './slices/error.slice';

export interface AppStore {
	pos: PosState;
	modal: ModalState;
	auth: AuthState;
	sales: SalesState;
	error: ErrorState;
}

export const store = configureStore({
	reducer: {
		pos: posReducer,
		modal: modalReducer,
		auth: authReducer,
		sales: salesReducer,
		error: errorReducer,
	},
});

export default store;
