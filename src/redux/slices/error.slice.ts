import { createSlice } from '@reduxjs/toolkit';

export interface ErrorState {
	message: string;
	status: number;
}

const initialState: ErrorState = {
	message: '',
	status: 0,
};

const errorSlice = createSlice({
	name: 'error',
	initialState,
	reducers: {
		setError: (state, action) => {
			state.message = action.payload.message;
			state.status = action.payload.status;
		},
		resetError: (state) => {
			state.message = initialState.message;
			state.status = initialState.status;
		},
	},
});

export const { setError, resetError } = errorSlice.actions;

export default errorSlice.reducer;
