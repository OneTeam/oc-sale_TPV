import { User } from '@/types';
import { createSlice } from '@reduxjs/toolkit';

export interface AuthState {
	user: User | null;
	database: string;
	isAuthenticated: boolean;
}

const initialState: AuthState = {
	user: null,
	database: '',
	isAuthenticated: false,
};

const authSlice = createSlice({
	name: 'auth',
	initialState,
	reducers: {
		setUser: (state, action) => {
			state.user = action.payload;
			state.isAuthenticated = true;
		},
		setDatabase: (state, action) => {
			state.database = action.payload;
		},
		resetAuth: (state) => {
			state.user = initialState.user;
			state.isAuthenticated = initialState.isAuthenticated;
			state.database = initialState.database;
		},
	},
});

export const { setUser, setDatabase, resetAuth } = authSlice.actions;

export default authSlice.reducer;
