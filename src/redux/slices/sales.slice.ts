import { Device, Sale, Table } from '@/types';
import { createSlice } from '@reduxjs/toolkit';

export interface SalesState {
	device: Device | null;
	orders: Sale[];
	order: Sale | null;
	tables: Table[];
}

const initialState: SalesState = {
	device: null,
	orders: [],
	order: null,
	tables: [],
};

const salesSlice = createSlice({
	name: 'sales',
	initialState,
	reducers: {
		setDevice: (state, action) => {
			state.device = action.payload;
		},
		setOrders: (state, action) => {
			state.orders = action.payload;
		},
		setOrder: (state, action) => {
			state.order = action.payload;
		},
		setTables: (state, action) => {
			state.tables = action.payload;
		},
		resetSales: (state) => {
			state.orders = initialState.orders;
			state.order = initialState.order;
			state.device = initialState.device;
			state.tables = initialState.tables;
		},
	},
});

export const {
	setDevice,
	setOrders,
	setOrder,
	setTables,
	resetSales,
} = salesSlice.actions;

export default salesSlice.reducer;
