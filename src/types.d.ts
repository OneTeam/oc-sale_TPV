import { AxiosResponse } from 'axios';

export type AxiosCall<T> = {
	call: Promise<AxiosResponse<T>>;
	controller: AbortController;
};

export type LoginRequest = {
	database: string;
	username: string;
	password: string;
};

export type LoginResponse = {
	token: string;
	user: UserResponse;
};

export type UserResponse = {
	id: number;
	name: string;
	username: string;
	avatar: string;
	role: string;
	device: DeviceResponse;
};

export type User = {
	id: number;
	name: string;
	avatar: string;
	username: string;
	role: string;
	device: Device;
};

export type DeviceResponse = {
	id: number;
	name: string;
};

export type Device = {
	id: number;
	name: string;
};

export type WarehouseResponse = {
	id: number;
	name: string;
};

export type Warehouse = {
	id: number;
	name: string;
};

export type TableResponse = {
	id: number;
	name: string;
	horizontal_position: number;
	vertical_position: number;
	busy: boolean;
	sale: number | null;
};

export type Table = {
	id: number;
	name: string;
	position: {
		x: number;
		y: number;
	};
	busy: boolean;
	sale: number;
};

export type SaleResponse = {
	id: number;
	sale_device: number;
	party: number;
	table: number | null;
	lines: ItemResponse[];
	total_amount: number;
	payment_method: string;
};

export type SaleRequest = {
	party: number;
	table?: number;
};

export type Sale = {
	id: number;
	device: number;
	party: number;
	table: number;
	payment: string;
	items: Item[];
	total: number;
};

export type ItemResponse = {
	id: number;
	product: number;
	quantity: number;
	unit_price: number;
	discount_amount: number;
	stock: number;
};

export type ItemRequest = {
	product: number;
	quantity: number;
	price: number;
	discount: number;
};

export type Item = {
	id: number;
	product: number;
	quantity: number;
	price: number;
	discount: number;
	stock: number;
};

export type ProductResponse = {
	id: number;
	name: string;
	code: string;
	image_url: string | null;
	list_price: number;
	categories: number[];
};

export type Product = {
	id: number;
	name: string;
	code: string;
	image: string | undefined;
	price: number;
	categories: number[];
};

export type CategoryResponse = {
	id: number;
	accounting: boolean;
	name: string;
	products: number;
};

export type Category = {
	id: number;
	name: string;
	products: number;
};

export type PartyResponse = {
	id: number;
	name: string;
};

export type Party = {
	id: number;
	name: string;
};

export type PrintResponse = AxiosResponse<Blob>;