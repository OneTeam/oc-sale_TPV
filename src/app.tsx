import { Routes as AppRoutes } from '@/constants';
import React from 'react';
import { Toaster } from 'react-hot-toast';
import {
	HashRouter,
	Navigate,
	Route,
	Routes,
} from 'react-router-dom';
import { PrivateRoute } from './guards';
import { Login, PointOfSale, Root, ServerError } from './pages';

export default function App() {
	const COMPANY = import.meta.env.VITE_COMPANY;

	React.useEffect(() => {
		document.title = COMPANY;
	}, [COMPANY]);

	return (
		<>
			<Toaster />
			<HashRouter>
				<Routes>
					<Route index element={<Root />} />
					<Route element={<PrivateRoute />}>
						<Route
							path={AppRoutes.POINT_OF_SALE}
							element={<PointOfSale />}
						/>
					</Route>
					<Route path={AppRoutes.LOGIN} element={<Login />} />
					<Route path={AppRoutes.ERROR} element={<ServerError />} />
					<Route
						path='*'
						element={<Navigate to={AppRoutes.ROOT} replace />}
					/>
				</Routes>
			</HashRouter>
		</>
	);
}
