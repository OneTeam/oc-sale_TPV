import { Table, TableResponse } from '../types';

export const tablesAdapter = (response: TableResponse[]) => {
	const result: Table[] = response.map((table: TableResponse) => {
		return {
			id: table.id,
			name: table.name,
			busy: table.busy,
			position: {
				x: table.horizontal_position,
				y: table.vertical_position,
			},
			sale: table.sale || 0,
		};
	});

	return result;
};
