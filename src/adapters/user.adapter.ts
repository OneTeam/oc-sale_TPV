import { User, UserResponse } from '@/types';
import { deviceAdapter } from './device.adapter';

export const userAdapter = (response: UserResponse) => {
	const result: User = {
		id: response.id,
		name: response.name,
		username: response.username,
		role: response.role,
		device: deviceAdapter(response.device),
		avatar: response.avatar
	};

	return result;
};
