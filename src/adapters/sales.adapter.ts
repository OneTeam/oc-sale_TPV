import { Sale, SaleResponse, Item, ItemResponse } from "@/types";

export const salesAdapter = (response: SaleResponse[]) => {

  if (response.length === 0) return [];

  const result: Sale[] = response.map((sale: SaleResponse) => {
    return {
      id: sale.id,
      device: sale.sale_device,
      party: sale.party,
      table: sale.table || 0,
      items: itemsAdapter(sale.lines),
      total: sale.total_amount,
      payment: ''
    };
  });

  return result;
};

export const saleAdapter = (response: SaleResponse) => {
  const result: Sale = {
    id: response.id,
    device: response.sale_device,
    party: response.party,
    table: response.table || 0,
    items: itemsAdapter(response.lines),
    total: response.total_amount,
    payment: ''
  };

  return result;
};

export const itemAdapter = (response: ItemResponse) => {
  const result: Item = {
    id: response.id,
    product: response.product,
    quantity: response.quantity,
    price: response.unit_price,
    discount: response.discount_amount,
    stock: response.stock
  };

  return result;
};

export const itemsAdapter = (response: ItemResponse[]) => {
  const result: Item[] = response.map((item: ItemResponse) => {
    return {
      id: item.id,
      product: item.product,
      quantity: item.quantity,
      price: item.unit_price,
      discount: item.discount_amount,
      stock: item.stock
    };
  });

  return result;
};