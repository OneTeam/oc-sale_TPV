import { Device, DeviceResponse } from "@/types"

export const devicesAdapter = (response: DeviceResponse[]) => {
  const result: Device[] = response.map((device: DeviceResponse) => {
    return {
      id: device.id,
      name: device.name
    }
  })

  return result
}

export const deviceAdapter = (response: DeviceResponse) => {
  const result: Device = {
    id: response.id,
    name: response.name
  }

  return result
}