import { Product, ProductResponse } from '../types';

export const productsAdapter = (response: ProductResponse[]) => {
	const result: Product[] = response.map(
		(product: ProductResponse) => {
			return {
				id: product.id,
				name: product.name,
				code: product.code,
				image: product.image_url || undefined,
				price: product.list_price,
				categories: product.categories,
			};
		},
	);

	return result;
};
